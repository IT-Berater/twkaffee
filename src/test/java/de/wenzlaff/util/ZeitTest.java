package de.wenzlaff.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Testklasse für die Zeit.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class ZeitTest {

	@Test
	public void testDatumZeitUTC() {

		String eingagsZeitpunktUTC = "2018-12-10 12:14:15 UTC";

		assertEquals(13, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getHour()); // da eine Stunde Plus
		assertEquals(14, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getMinute());
		assertEquals(15, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getSecond());

		assertEquals("Europe/Berlin", Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getZone().getId()); // Zeitzone
																											// nicht
																											// mehr UTC
		assertEquals(10, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getDayOfMonth()); // Tag
		assertEquals(12, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getMonthValue()); // Achtung! Monat der Wert,
																								// nicht getMonth!
		assertEquals(2018, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getYear());
	}

	@Test
	public void testDatumZeitUTCNextDay() {

		String eingagsZeitpunktUTC = "2018-12-10 23:14:15 UTC";

		assertEquals(0, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getHour()); // da eine Stunde Plus
		assertEquals(14, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getMinute());
		assertEquals(15, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getSecond());

		assertEquals("Europe/Berlin", Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getZone().getId()); // Zeitzone
																											// nicht
																											// mehr UTC
		assertEquals(11, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getDayOfMonth()); // ein Tag weiter
		assertEquals(12, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getMonthValue()); // Achtung! Monat der Wert,
																								// nicht getMonth!
		assertEquals(2018, Zeit.getZonedDateTimeFromUTC(eingagsZeitpunktUTC).getYear());
	}

}
