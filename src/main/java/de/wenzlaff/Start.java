package de.wenzlaff;

import java.io.File;
import java.io.FileInputStream;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import de.wenzlaff.model.Kaffee;

/**
 * Start der Kaffe Auswertung
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Start {

	private static final Logger LOG = LogManager.getLogger(Start.class);

	private static final int SPALTE_ZEITPUNKT = 0;
	private static final int SPALTE_EINTRAG_NR = 1;
	private static final int SPALTE_ANZAHL = 2;

	private static List<Kaffee> kaffeeItems;

	/**
	 * Der Start Punkt.
	 * 
	 * @param args die Excel Datei als Parameter.
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		LOG.info("Programm Start ... ");

		String excelDateiName = "kaffee.xlsx";

		if (args.length != 1) {
			hilfeTextAusgeben();
		} else {
			excelDateiName = args[0];
		}

		LOG.info("Verwende Input Excel Datei " + excelDateiName);

		if (!new File(excelDateiName).exists()) {
			LOG.error("Datei " + excelDateiName + " gibt es nicht. Programm beendet.");
			return;
		}

		kaffeeItems = new ArrayList<Kaffee>();

		LOG.info("Lese alle Zeilen aus der Excel Datei " + excelDateiName);
		FileInputStream inputStream = new FileInputStream(new File(excelDateiName));

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();

		int maxSpalten = firstSheet.getRow(0).getLastCellNum();
		LOG.info("Anzahl der Spalten der Tabelle: " + maxSpalten);
		String sheetName = firstSheet.getSheetName();
		LOG.info("Verwende Blatt " + sheetName);

		while (iterator.hasNext()) { // �ber alle Zeilen
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			Kaffee kaffee = new Kaffee();

			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				int columnIndex = nextCell.getColumnIndex();

				Object wert = getCellValue(nextCell);
				if (columnIndex == SPALTE_ZEITPUNKT) {
					kaffee.setErstelltAm(wert.toString());// 2017-04-05 17:20:41 UTC
				} else if (columnIndex == SPALTE_EINTRAG_NR) {
					kaffee.setEintragNr(wert.toString());
				} else if (columnIndex == SPALTE_ANZAHL) {
					kaffee.setAnzahlKaffee(wert.toString());
				}
			}
			try {
				if (isTitelzeile(kaffee)) {
					kaffeeItems.add(kaffee);
					LOG.info("Eingelesen " + kaffee);
				}
			} catch (Exception e) {
				System.err.println("Fehler in Zeile: " + kaffee + " Exception:" + e);
			}
		}
		workbook.close();
		inputStream.close();

		LOG.info(kaffeeItems.size() + " gelesene Zeilen aus der Tabelle " + excelDateiName);

		auswertung();
	}

	private static void auswertung() {

		kaffeeItems.stream().filter(k -> k.getAnzahlKaffeeAsInt() == 8).forEach(k -> LOG.info(k));

		for (int i = 1; i < 11; i++) {
			final int j = i;
			long anzahl = kaffeeItems.stream().filter(k -> k.getAnzahlKaffeeAsInt() == j).count();

			LOG.info(j + " Anzahl Kaffee pro Tag: " + anzahl);
		}

		// Erster Eintrag
		// 2017-04-05 17:20:41 UTC 1 1

		// Letzter Eintrag
		// 2018-12-09 11:43:37 UTC 2423 7

		long anzahlTage = ChronoUnit.DAYS.between(kaffeeItems.get(0).getErstelltAmLokal(),
				kaffeeItems.get(kaffeeItems.size() - 1).getErstelltAmLokal());

		LOG.info("--------------------");
		// gehe �ber den Tag solange der n�chste gleich ist

		int tage = 0;
		int summe = 0;

		for (int i = 0; i < kaffeeItems.size() - 1; i++) {
			if (kaffeeItems.get(i).getErstelltAmLokal().getDayOfMonth() != kaffeeItems.get(i + 1).getErstelltAmLokal()
					.getDayOfMonth()) {
				LOG.info(kaffeeItems.get(i));
				tage++;
				summe += kaffeeItems.get(i).getAnzahlKaffeeAsInt();
			}
		}

		LOG.info("-------------------- Der 1. Kaffee am Tag");
		kaffeeItems.stream().filter(k -> k.getAnzahlKaffee().equals("1.0"))
				.forEach(k -> LOG.info(k.getErstelltAmLokal().getHour() + ":" + k.getErstelltAmLokal().getMinute()));

		LOG.info("-------------------- Der Letzte Kaffee am Tag");
		for (int i = 0; i < kaffeeItems.size() - 1; i++) {
			if (kaffeeItems.get(i).getErstelltAmLokal().getDayOfMonth() != kaffeeItems.get(i + 1).getErstelltAmLokal()
					.getDayOfMonth()) {
				LOG.info(kaffeeItems.get(i).getErstelltAmLokal().getHour() + ":"
						+ kaffeeItems.get(i).getErstelltAmLokal().getMinute());
			}
		}

		LOG.info("--------------------");
		LOG.info("Summe: " + summe + " Durchschnitt: " + summe / tage + " Tassen Kaffee pro Tag");
		LOG.info("Anzahl Tage: " + tage);
		LOG.info("Anzahl der Tage die gemessen wurden: " + anzahlTage);

	}

	private static Object getCellValue(Cell cell) {

		if (cell.getCellTypeEnum() == CellType.STRING) {
			return cell.getStringCellValue();
		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
			return cell.getBooleanCellValue();
		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
			return cell.getNumericCellValue();
		}
		return null;
	}

	private static boolean isTitelzeile(Kaffee kaffeeEintrag) {
		// keine Titelzeile einlesen, das heisst �berpr�fe auf Nr in erster Spalte!
		return !kaffeeEintrag.getEintragNr().equals("1.0");
	}

	private static void hilfeTextAusgeben() {
		LOG.info("Programmaufruf: de.wenzlaff.Start [Excel Dateiname] ");
		LOG.info("Aufruf z.B.: de.wenzlaff.Start exceldatei.xlsx");

	}

}
