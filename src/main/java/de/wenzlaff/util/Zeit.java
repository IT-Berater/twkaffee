package de.wenzlaff.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Zeitumrechnung
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Zeit {

	/**
	 * Liefer die Zeitzone mit Datum und Uhrzeit aus einem UTC String. Umgerechnet
	 * auf Europa (+1 Stunde).
	 * 
	 * Die Koordinierte Weltzeit (UTC) ist keine Zeitzone, sondern ein Zeitstandard
	 * f�r die Berechnung von Ortszeiten in Zeitzonen weltweit. Veraltet: (aber
	 * gleiche wie UTC) Greenwich Mean Time (GMT) ist eine Zeitzone. Sie orientiert
	 * sich an der UTC und wird heute in mehreren L�ndern noch offiziell verwendet,
	 * darunter Gro�britannien.
	 * 
	 * @param eingagsZeitpunktUTC in der Form yyyy-MM-dd HH:mm:ss UTC z.B.
	 *                            2018-12-10 12:00:00 UTC
	 * @return die ZonedDateTime auf Europa umgerechnet (+1 Stunde) z.B. 2018-12-10
	 *         13:00:00 Europa/Berlin
	 */
	public static ZonedDateTime getZonedDateTimeFromUTC(String eingagsZeitpunktUTC) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss 'UTC'");

		LocalDateTime dateTimeUTC = LocalDateTime.parse(eingagsZeitpunktUTC, formatter);

		ZonedDateTime inputUTC = ZonedDateTime.of(dateTimeUTC, ZoneId.of("UTC"));

		ZonedDateTime lokalEuropa = inputUTC.withZoneSameInstant(ZoneId.of("Europe/Berlin")); // UTC in Europa Zeitzone
																								// konvertieren

		return lokalEuropa;
	}
}
