package de.wenzlaff.model;

import java.time.ZonedDateTime;

import de.wenzlaff.util.Zeit;

/**
 * Kaffee BE
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Kaffee {

	// created_at,entry_id,field1
	// 2017-04-05 17:20:41 UTC,1,1
	// 2017-04-06 04:27:28 UTC,2,1

	/** Die UTC Eingangsdaten. */
	private String erstelltAm; // 2017-04-05 17:20:41 UTC
	/**
	 * H�lt die Lokale Zeitzone von Europa/Berlin. D.h. +1 von UTC der erstellten
	 * Eingangsdaten.
	 **/
	private ZonedDateTime erstelltAmLokal;

	private String eintragNr;
	private String anzahlKaffee;

	public Kaffee() {

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Kaffee [");
		if (erstelltAmLokal != null) {
			builder.append("erstelltAm=");
			builder.append(erstelltAmLokal.toLocalDateTime());
			builder.append(", ");
		}
		if (anzahlKaffee != null) {
			builder.append("anzahlKaffee=");
			builder.append(anzahlKaffee);
			builder.append(", ");
		}
		if (eintragNr != null) {
			builder.append("eintragNr=");
			builder.append(eintragNr);
		}

		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eintragNr == null) ? 0 : eintragNr.hashCode());
		result = prime * result + ((erstelltAmLokal == null) ? 0 : erstelltAmLokal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kaffee other = (Kaffee) obj;
		if (eintragNr == null) {
			if (other.eintragNr != null)
				return false;
		} else if (!eintragNr.equals(other.eintragNr))
			return false;
		if (erstelltAmLokal == null) {
			if (other.erstelltAmLokal != null)
				return false;
		} else if (!erstelltAmLokal.equals(other.erstelltAmLokal))
			return false;
		return true;
	}

	public void setErstelltAm(String erstelltAm) {
		erstelltAmLokal = Zeit.getZonedDateTimeFromUTC(erstelltAm);
		this.erstelltAm = erstelltAm;
	}

	public String getErstelltAm() {
		return erstelltAm;
	}

	public ZonedDateTime getErstelltAmLokal() {
		return erstelltAmLokal;
	}

	public String getEintragNr() {
		return eintragNr;
	}

	public void setEintragNr(String eintragNr) {
		this.eintragNr = eintragNr;
	}

	public String getAnzahlKaffee() {
		return anzahlKaffee;
	}

	public int getAnzahlKaffeeAsInt() {
		return Double.valueOf(anzahlKaffee).intValue();
	}

	public void setAnzahlKaffee(String anzahlKaffee) {
		this.anzahlKaffee = anzahlKaffee;
	}

}
